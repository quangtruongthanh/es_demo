﻿using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace ES_Demo
{
    class Program
    {
        public static Uri EsNode;
        public static ConnectionSettings EsConfig;
        public static ElasticClient EsClient;
        static void Main(string[] args)
        {
            EsNode = new Uri(Config.ElasticSearchUri);
            EsConfig = new ConnectionSettings(EsNode);

            if (!string.IsNullOrWhiteSpace(Config.ElasticSearchUser))
            {
                EsConfig.BasicAuthentication(Config.ElasticSearchUser, Config.ElasticSearchPass);
            }

            EsClient = new ElasticClient(EsConfig);
            EsClient.DeleteIndex(Config.Index);
            if (!EsClient.IndexExists(Config.Index).Exists)
            {
                EsClient.CreateIndex(Config.Index, c => c
                .Mappings(m => m.Map<JsonData>(mp => mp.AutoMap()).Map<JsonDataId>(mp => mp.AutoMap())));
            }

            InsertDocument();

        }

        private static void InsertDocument()
        {
            using (StreamReader sr = new StreamReader(Config.FilePath))
            {
                string jsonString = sr.ReadToEnd();
                var datas = JsonConvert.DeserializeObject<List<JsonData>>(jsonString);
                foreach(var data in datas)
                {
                    var result = EsClient.Index(data, i => i
                                      .Index(Config.Index)
                                      .Type(Config.Type)
                                      .Id(data.ID.Oid).Refresh(Elasticsearch.Net.Refresh.True)
                                       );
                }

            }

            //to view all indexed data: http://localhost:9200/collegehippo/scholarship/_search?pretty=true&q=*:*
        }


    }

    public static class Config
    {
        public static string FilePath = ConfigurationManager.AppSettings["FilePath"];
        public static string ElasticSearchUri = ConfigurationManager.AppSettings["ElasticSearchUri"];
        public static string ElasticSearchUser = ConfigurationManager.AppSettings["ElasticSearchUser"];
        public static string ElasticSearchPass = ConfigurationManager.AppSettings["ElasticSearchPass"];
        public static string Index = "collegehippo";
        public static string Type = "scholarship";
    }
    public class JsonDataId
    {
        [JsonProperty("$oid")]
        [Text(Name = "$oid")]
        public string Oid { set; get; }
    }
    public class JsonData
    {
        [JsonProperty("_id")]
        [Text(Name = "id")]
        public JsonDataId ID { set; get; }
        [Text(Name = "STD_TEXT_ID")]
        public string STD_TEXT_ID { set; get; }
        [Text(Name = "STD_UNIV_CRAWL_ID")]
        public string STD_UNIV_CRAWL_ID { set; get; }
        [Text(Name = "UNITID")]
        public string UNITID { set; get; }
        [Text(Name = "NAME")]
        public string NAME { set; get; }
        [Text(Name = "COURSE_URL")]
        public string COURSE_URL { set; get; }
        [Text(Name = "TITLE")]
        public string TITLE { set; get; }
        [Text(Name = "TITLE_MODIF")]
        public string TITLE_MODIF { set; get; }
        [Text(Name = "DESCRIPTION")]
        public string DESCRIPTION { set; get; }
        [Text(Name = "COURSE_TYPE")]
        public string COURSE_TYPE { set; get; }
        [Text(Name = "DEGREE_TYPE")]
        public string DEGREE_TYPE { set; get; }
        [Text(Name = "CIPCODE")]
        public string CIPCODE { set; get; }
        [Text(Name = "RATING")]
        public string RATING { set; get; }
        [Text(Name = "URL_EXT_TEXT")]
        public string URL_EXT_TEXT { set; get; }
        [Text(Name = "TEXT_SUMMARY")]
        public string TEXT_SUMMARY { set; get; }
        [Text(Name = "TEXT_MODIF")]
        public string TEXT_MODIF { set; get; }
        [Text(Name = "TEXT_SUMMARY_MODIF")]
        public string TEXT_SUMMARY_MODIF { set; get; }
        [Text(Name = "KEYWORD")]
        public string KEYWORD { set; get; }
        [Text(Name = "Active")]
        public string ACTIVE { set; get; }
        [Text(Name = "UPDATED_DATE")]
        public string UPDATED_DATE { set; get; }

    }
}
